# soru-gitpod

## How to update this image to the latest dailynet?

1. go to https://teztnets.xyz/dailynet-about
2. write down commit hash, e.g. the info Tezos docker build is tezos/tezos:master_23aca631_20230404202335 today, so the commit hash is 23aca631
3. go to https://gitlab.com/tezos/tezos/-/commit/THE_COMMIT_HASH, e.g. https://gitlab.com/tezos/tezos/-/commit/23aca631
4. In the insert at the top, there is Pipeline #828138109 passed with stages ✅✅✅✅✅✅ in 66 minutes and 32 seconds  Click on the stage build (second check mark) → build:static-x86_64-linux-binaries (third build job in the drop-down list of that second check mark)
5. The job number is now in the URL, e.g. https://gitlab.com/tezos/tezos/-/jobs/4061952837 means the job number is 4061952837.
6. Go to https://gitlab.com/ligo.suzanne.soy/soru-gitpod/, edit the file gitpod.Dockerfile
7. At the top of the file, change the build number, e.g. change ENV TEZOS_BUILD_JOB=4056359113 to ENV TEZOS_BUILD_JOB=4061952837