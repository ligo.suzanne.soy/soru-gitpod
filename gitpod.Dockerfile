ENV TEZOS_BUILD_JOB=4056359113
FROM gitpod/workspace-full:latest

# Install ligo

FROM ligolang/ligo:0.58.0 as ligo
FROM gitpod/workspace-full:latest

RUN npm i -g @esy-nightly/esy
COPY --from=ligo /root/ligo /usr/local/bin/ligo

# Install octez-client

#FROM tezos/tezos:master_a19bbd17_20230329225603 as tezos
#FROM gitpod/workspace-full:latest
#COPY --from=tezos /usr/local/bin/* /usr/local/bin
ENV DEBIAN_FRONTEND=noninteractive
RUN sudo apt update && sudo apt install -y \
            autoconf \
            bash \
            curl \
            fd-find \
            gawk \
            git \
            g++ \
            jq \
            libev4 \
            libgmp-dev \
            libhidapi-dev \
            libsodium-dev \
            libsodium23 \
            libtool \
            make \
            m4 \
            net-tools \
            opam \
            openssl \
            pkg-config \
            python-is-python3 \
            python3-pip \
            ruby \
            ruby-json

RUN for f in \
        octez-accuser-PtLimaPt \
        octez-accuser-PtMumbai \
        octez-accuser-alpha \
        octez-admin-client \
        octez-baker-PtLimaPt \
        octez-baker-PtMumbai \
        octez-baker-alpha \
        octez-client \
        octez-codec \
        octez-dac-node \
        octez-dal-node \
        octez-evm-proxy-server \
        octez-node \
        octez-proxy-server \
        octez-signer \
        octez-smart-rollup-client-PtMumbai \
        octez-smart-rollup-client-alpha \
        octez-smart-rollup-node-PtMumbai \
        octez-smart-rollup-node-alpha \
        octez-smart-rollup-wasm-debugger \
        octez-tx-rollup-client-PtLimaPt \
        octez-tx-rollup-node-PtLimaPt \
    ; do \
                wget "https://gitlab.com/tezos/tezos/-/jobs/$TEZOS_BUILD_JOB/artifacts/raw/octez-binaries/x86_64/$f"; \
                chmod +x "$f"; \
                sudo mv "$f" /usr/local/bin/; \
    done

# BEGIN ZCASH PARAMS
# Adapted from https://gitlab.com/tezos/flextesa/-/blob/097ef5d5385105f4b2e1f25c23ef767ee05da3e7/src/scripts/get-zcash-params.sh
ENV SAPLING_SPEND='sapling-spend.params'
ENV SAPLING_OUTPUT='sapling-output.params'
# ENV SAPLING_SPROUT_GROTH16_NAME='sprout-groth16.params'
ENV DOWNLOAD_URL="https://download.z.cash/downloads"
ENV destination=/usr/share/zcash-params
RUN sudo mkdir -p "$destination"
RUN sudo curl --output "$destination/$SAPLING_OUTPUT" -L "$DOWNLOAD_URL/$SAPLING_OUTPUT"
RUN sudo curl --output "$destination/$SAPLING_SPEND" -L "$DOWNLOAD_URL/$SAPLING_SPEND"
# END ZCASH PARAMS

#RUN git clone git@gitlab.com:tezos/tezos.git
#RUN cd tezos
#RUN git checkout a19bbd17
#RUN opam init # if this is your first time using OPAM
#RUN make build-deps
#RUN eval $(opam env)
#RUN make
#RUN export PATH=$HOME/tezos/_build/install/default/bin/:$PATH

# Install tezos-client

#RUN sudo add-apt-repository ppa:serokell/tezos && sudo apt-get update
#RUN sudo touch /.containerenv
#RUN sudo apt-get install -y apt-transport-https tezos-client tezos-node tezos-tx-rollup-client-ptlimapt tezos-tx-rollup-node-ptlimapt

# Install completium-cli

#RUN npm i '@completium/completium-cli@0.4.39' -g
#RUN completium-cli init
#RUN completium-cli mockup init

# Download NL's Michelson vs-studio plugin
RUN sudo wget -q http://france-ioi.org/extension.vsix -O /home/.2HzpexT7tKMixL.vsix
#RUN code --install-extension /tmp/.2HzpexT7tKMixL.vsix
