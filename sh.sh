#!/usr/bin/env bash

#set -euET -o pipefail

# version from https://teztnets.xyz/dailynet-about
# sudo docker run -it --entrypoint=/bin/sh tezos/tezos:master_6a1cbb8a_20230220162747

#set -x

if apk version >/dev/null 2>&1; then
  sudo apk add jq
fi

export ONODE_DIR="$(mktemp -d "onode_dir_XXXXXXXXXX")"
export OCLIENT_DIR="$(mktemp -d "oclient_dir_XXXXXXXXXX")"
export ROLLUP_NODE_DIR="$(mktemp -d "rollup_node_dir_XXXXXXXXXX")"
export TEZOS_CLIENT_DIR="$OCLIENT_DIR"
export OCTEZ_CLIENT_DIR="$OCLIENT_DIR"
export NETWORK='https://teztnets.xyz/dailynet-'"$(date +%Y-%m-%d)"
export OPERATOR='Suzanne'
octez-node config init --data-dir "${ONODE_DIR}" --network "${NETWORK}"
# TODO: was 0, check if it works with 1
jq '. += { "shell": { "chain_validator": { "synchronisation_threshold": 1 } } }' "$ONODE_DIR/config.json" > "$ONODE_DIR/config2.json"
mv "$ONODE_DIR/config2.json" "$ONODE_DIR/config.json"
octez-node run --data-dir "${ONODE_DIR}" --network "${NETWORK}" --rpc-addr 127.0.0.1 > onode.log 2>&1 &

printf 'Waiting for the node to start and protocol to switch to proto alpha'\\n
while test "$(TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=yes octez-client rpc get /chains/main/blocks/head/protocols 2>/dev/null | jq -r .protocol)" \
           != 'ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK'; do
  printf .
  sleep 1
done
octez-client bootstrapped

octez-client gen keys "$OPERATOR"
export OPERATOR_ADDR="$(octez-client show address "$OPERATOR" | sed -ne 's/^Hash: //p')"
printf 'Please use the dailynet faucet at https://teztnets.xyz/ to add at least 10000ꜩ to the account %s'\\n "$OPERATOR_ADDR"
printf 'Waiting for account balance to be 10000ꜩ or more'\\n
balance=0
while test "$balance" -lt 10000; do
  balance="$(TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=yes octez-client get balance for "${OPERATOR_ADDR}" | sed -e 's/ ꜩ$//')"
  printf '\r\033[KBalance of %s : %s ꜩ (last checked: %s)' "$OPERATOR_ADDR" "$balance" "$(date +%H:%M:%S)"
  sleep 1
done
printf \\n

export KERNEL="$(wget https://tezos.gitlab.io/_downloads/3dc4fd44d97861f183e18fda0ee8a9ae/sr_boot_kernel.sh -O- \
                 | sed -ne 's/^export KERNEL="//p' | sed -e 's/"$//')"
# export KERNEL=0061736d0100000001280760037f7f7f017f60027f7f017f60057f7f7f7f7f017f60017f0060017f017f60027f7f0060000002610311736d6172745f726f6c6c75705f636f72650a726561645f696e707574000011736d6172745f726f6c6c75705f636f72650c77726974655f6f7574707574000111736d6172745f726f6c6c75705f636f72650b73746f72655f77726974650002030504030405060503010001071402036d656d02000a6b65726e656c5f72756e00060aa401042a01027f41fa002f0100210120002f010021022001200247044041e4004112410041e400410010021a0b0b0800200041c4006b0b5001057f41fe002d0000210341fc002f0100210220002d0000210420002f0100210520011004210620042003460440200041016a200141016b10011a0520052002460440200041076a200610011a0b0b0b1d01017f41dc0141840241901c100021004184022000100541840210030b0b38050041e4000b122f6b65726e656c2f656e762f7265626f6f740041f8000b0200010041fa000b0200020041fc000b0200000041fe000b0101

SOR_ADDR="$(octez-client originate smart rollup from "${OPERATOR_ADDR}" \
  of kind wasm_2_0_0 \
  of type bytes \
  with kernel "${KERNEL}" \
  --burn-cap 999 \
| sed -ne "s/^\s*Address: //p" | grep '^sr1')"

# Create smart rollup config
octez-smart-rollup-node-alpha --base-dir "${OCLIENT_DIR}" \
                 init operator config for "${SOR_ADDR}" \
                 with operators "${OPERATOR_ADDR}" \
                 --data-dir "${ROLLUP_NODE_DIR}"

# Run smart rollup node (it runs the computation, consuming publicly-posted messages
octez-smart-rollup-node-alpha -d "${OCLIENT_DIR}" run --data-dir "${ROLLUP_NODE_DIR}" > rollup-node.log 2>&1 &

# Originate a contract which can recieve messages from a rollup
CONTRACT="$(octez-client -d "${OCLIENT_DIR}" -p ProtoALphaAL \
  originate contract go transferring 1 from "${OPERATOR_ADDR}" \
  running 'parameter string; storage string; code {CAR; NIL operation; PAIR};' \
  --init '""' --burn-cap 0.4\
| sed -ne "s/^New contract \(KT1[^ ]*\) originated./\1/p")"

# Encode the rollup outbox message (our rollup just copies its inbox to its outbox, so we need the message to already be encoded)
MESSAGE='[
  {
    "destination" : "'"${CONTRACT}"'",
    "parameters" : "\"Hello world\""
  }
]'
#  "entrypoint" : "default"


EMESSAGE=$(octez-smart-rollup-client-alpha encode outbox message "${MESSAGE}")

get_level() {
  TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=yes octez-client rpc get /chains/main/blocks/head | jq .header.level
}

last_uncemented=$(get_level)

# Send the pre-encoded message to the smart rollup
octez-client -d "${OCLIENT_DIR}" -p ProtoALphaAL \
 send smart rollup message "hex:[ \"${EMESSAGE}\" ]" \
 from "${OPERATOR_ADDR}"

circumvent_bug() {
  jq 'if .parameters | has("string") then .parameters = (.parameters.string | tojson) else . end'
}

execute_outbox_messages() {
  #wait for level to be 40 more than level at which commitment is made
  while sleep 1; do
    printf '%s' "$(date)"
    for level in $(seq "$last_uncemented" "$(get_level)"); do
      printf '\033[KChecking block at level %s for cementation and outbox messages\r' "$level"
      txs="$(octez-smart-rollup-client-alpha rpc get /global/block/cemented/outbox/"$level"/messages \
             | jq '.[].message.transactions[] | tojson')"
      if test -n "$txs" ; then
        last_uncemented="$((level+1))"
        message_number=0
        printf '%s\n' "$txs" | while read -r line; do
          printf '\033[K'
          printf '\nExecuted outbox message %s of cemented block (at level %s, current: %s).\n' "$message_number" "$level" "$(get_level)"

          tx="$(printf '%s\n' "$line" | jq fromjson | circumvent_bug)"
          PROOF="$(octez-smart-rollup-client-alpha get proof for message "$message_number" of outbox at level "$level" transferring "[ $tx ]")"
          message_number=$((message_number+1)) # TODO: unchecked

          # ignore failures, could be that someone else executed the outbox message already
          octez-client -d "${OCLIENT_DIR}" -p ProtoALphaAL \
           execute outbox message of smart rollup "${SOR_ADDR}" \
           from "${OPERATOR_ADDR}" for commitment hash "$(printf %s "$PROOF" | jq -r '.commitment_hash')" \
           and output proof "$(printf %s "$PROOF" | jq -r '.proof')" --burn-cap 1 || true

          printf 'Storage of %s:\n  %s\n' "$CONTRACT" "$(octez-client get contract storage for "$CONTRACT")"
        done
      fi
    done
  done
}

execute_outbox_messages > execute_outbox_messages.log 2>&1 &
